<?php

// var url = require("url");
// var queryString = require("querystring");

$exports['parse'] = function($url) {
  //url.parse;
  $parsed = parse_url($url);
  return [
    'protocol' => $parsed['scheme'] . ':',
    'slashes' => true, //TODO ever false
    'auth' => $parsed['user'] ? $parsed['user'].":".$parsed['pass'] : null,
    'host' => $parsed['host'],
    'port' => $parsed['port'],
    'hostname' => $parsed['hostname'],
    'hash' => substr($parsed['hash'], 1),
    'search' => '?' . $parsed['query'],
    'query' => $parsed['query'],
    'pathname' => $parsed['path'],
    'path' => $parsed['path'] . "?" . $parsed['query'],
    'href' => $url
  ];
};

$exports['format'] = function($parsed) {
  //url.format;
  return
    (isset($parts['protocol']) ? "{$parts['protocol']}:" : '') . 
    ((isset($parts['auth']) || isset($parts['host'])) ? '//' : '') . 
    (isset($parts['auth']) ? "{$parts['auth']}" : '') . 
    (isset($parts['auth']) ? '@' : '') . 
    (isset($parts['host']) ? "{$parts['host']}" : '') . 
    (isset($parts['port']) ? ":{$parts['port']}" : '') . 
    (isset($parts['path']) ? "{$parts['path']}" : '') . 
    (isset($parts['hash']) ? "#{$parts['hash']}" : '');
};

$exports['resolve'] = function ($from) {
  return function ($to) use (&$from) {
    // TODO url.resolve();
    /* return if already absolute URL */
    if (parse_url($to, PHP_URL_SCHEME) != '' || substr($to, 0, 2) == '//') return $to;

    /* queries and anchors */
    if ($to[0]=='#' || $to[0]=='?') return $from.$to;

    /* parse base URL and convert to local variables:
      $scheme, $host, $path */
    extract(parse_url($from));

    /* remove non-directory element from path */
    $path = preg_replace('#/[^/]*$#', '', $path);

    /* destroy path if relative url points to root */
    if ($to[0] == '/') $path = '';

    /* dirty absolute URL */
    $abs = "$host$path/$to";

    /* replace '//' or '/./' or '/foo/../' with '/' */
    $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
    for($n=1; $n>0; $abs=preg_replace($re, '/', $abs, -1, $n)) {}

    /* absolute URL is ready! */
    return $scheme.'://'.$abs;
  };
};

$exports['parseQueryString'] = function($s) {
  $arr = [];
  parse_str($s, $arr);
  return $arr;
};

$exports['toQueryString'] = function($e) {
  return urlencode($e); //TODO fix
};
